#!/usr/bin/python

from gpsd import Gps
from gpsdclientsock import *
import time
import datetime
import re
import argparse
import threading
from baselogger import Logger


class NMEAServer(object):
    """
    A server to stream data from a file or if via a gpsd service a json or nmea gps stream
    """
    
    def __init__(self, address="0.0.0.0", port=2948, print_to_screen=False):
        """
        A nmea streaming service using either dummy data or a gpsd instance for real time data
        :param address: broadcast address 0.0.0.0 default
        :type address: str
        :param port: Broadcast port 2948 default
        :type port: int
        :param print_to_screen: print raw data to screen
        :type print_to_screen: bool
        """
        # create logger
        self.log = Logger("NMEAServer", "DEBUG", "nmeaserver_log")
        
        # Create a TCP/IP socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._print_to_screen = print_to_screen
        self.success = True
        # Bind the socket to the port
        server_address = (address, port)
        self.log.file_console.info('starting up on {0} port {1}'.format(server_address[0], server_address[1]))
        self.connections = []
        self.is_running = True
        
        while True:
            try:
                self.sock.bind(server_address)
            except socket.error as e:
                self.log.file_console.warning(e)
                try:
                    time.sleep(5)
                except KeyboardInterrupt:
                    # self.sock.close()
                    self.success = False
                    raise
            else:
                self.sock.listen(100)
                break
        self.num_satellites = 0
        self.hdop = ''
        self.is_running = True
    
    def close(self):
        """safely closes sockets on exit"""
        self.is_running = False
        for i, (con, address) in enumerate(self.connections):
            con.shutdown(socket.SHUT_RDWR)
            con.close()
            self.connections.pop(i)
            self.log.file_console.info("Closed {}:{}".format(*address))
        
        time.sleep(1)
        self.sock.close()
    
    def __del__(self):
        self.close()
    
    def start_accept(self):
        """
        Start accepting connections in a thread
        :return: None
        """
        sa = threading.Thread(target=self.socket_accept, name='socketaccept')
        sa.daemon = True
        sa.start()
    
    def socket_accept(self):
        """
        Accepts connections
        """
        self.log.file_console.info("watching connections")
        while self.is_running:
            try:
                connection, client_address = self.sock.accept()
                self.log.file_console.info(
                        "Connection accepted from {}:{}".format(client_address[0], client_address[1]))
                self.connections.append([connection, client_address])
            except socket.error:
                pass
            except KeyboardInterrupt:
                self.sock.close()
                raise
        self.log.file_console.info("Stopped Accepting Connections")
    
    def run(self, tojson=False):
        """
        Runs the streamer for gpsd interface
        :param tojson: run json output
        :type tojson: bool
        :return: 0 when completed or cancelled
        """
        # start accepting connections
        self.start_accept()
        
        opts = {"_verbose": False}
        # opts["host"] = ""
        # opts["port"] = ""
        
        session = Gps(**opts)
        session.stream(WATCH_ENABLE)
        while True:
            waiting_msg = True
            while len(self.connections) == 0:
                if waiting_msg:
                    self.log.file_console.info("No Connections, waiting for connections")
                    waiting_msg = False
                time.sleep(1)
            conn_num = 0
            try:
                for report in session:
                    if tojson:
                        for con, client_add in self.connections:
                            con.sendall(report)
                        if self._print_to_screen:
                            print(report)
                    else:
                        nmea_strings = self.dict_to_nmea(report)
                        for nmea_str in nmea_strings:
                            if nmea_str is None:
                                continue
                            elif isinstance(nmea_str, str):
                                if self._print_to_screen:
                                    print(nmea_str)
                                for i, (con, client_add) in enumerate(self.connections):
                                    conn_num = i
                                    con.sendall(nmea_str)
                            elif isinstance(nmea_str, list):
                                for nm_str in nmea_str:
                                    if self._print_to_screen:
                                        print(nm_str)
                                    for i, (con, client_add) in enumerate(self.connections):
                                        conn_num = i
                                        con.sendall(nm_str)
            except socket.error as exc:
                self.log.file.warning("Caught exception socket.error : {}s".format(exc))
                bad_con, address = self.connections.pop(conn_num)
                ip, prt = address
                try:
                    bad_con.shutdown(socket.SHUT_RDWR)
                    bad_con.close()
                except socket.error:
                    self.log.file_console.warning("Failed to close connection to {}:{}".format(ip, prt))
                else:
                    self.log.file_console.info("Closed connection to {}:{}".format(ip, prt))
            except KeyboardInterrupt:
                self.log.file_console.warning("Exiting from keyboard interrupt")
                raise
    
    def dict_to_gga_rmc(self, tpv_dict):
        """
        Converts gpsd TPV class dict to to GGS and RMA data
        :param tpv_dict:
        :return: GGA , RMC strings
        """
        dsat = self.num_satellites
        dhdop = self.hdop
        dlat = getattr(tpv_dict, 'lat', '')
        dfix = 1 if dlat else 0
        dlon = getattr(tpv_dict, 'lon', '')
        dtime = getattr(tpv_dict, 'time', '')
        dalt = getattr(tpv_dict, 'alt', '')
        # depy = getattr(tpv_dict, 'epy', '')
        dspeed = getattr(tpv_dict, 'speed', '')
        dtrack = getattr(tpv_dict, 'track', '')
        
        # preformat to remove issues when data isn't available
        if dspeed != '':
            dspeed = "{0:3.1f}".format(dspeed)
        
        if dtrack != '':
            dtrack = "{0:.1f}".format(dtrack)
        
        date = ''
        if dtime:
            date = "{}{}{}".format(dtime[8:10], dtime[5:7], dtime[2:4])
            dtime = "{}{}{}.000".format(dtime[11:13], dtime[14:16], dtime[17:19])
        if dlon:
            d, m, sd = self.dd2dms(dlon)
            sd = int(str(sd).replace(".", ""))
            dlon = "{}{:02d}.{:03d}".format(d, m, sd)
        if dlat:
            d, m, sd = self.dd2dms(dlat)
            sd = int(str(sd).replace(".", ""))
            dlat = "{}{:02d}.{:03d}".format(d, m, sd)
        gprmc_base = "GPRMC,{0},A,{1},N,{2},E,{3},{4},{5},,W".format(dtime, dlat, dlon, dspeed, dtrack, date)
        gpgga_base = "GPGGA,{0},{1},N,{2},E,{3},{4},{5},{6},M,{6},M,,".format(dtime, dlat, dlon, dfix, dsat, dhdop,
                                                                              dalt)
        
        rmc_chk_str, rmc_chk_sum, rmc_chk_hex = self.checksum(gprmc_base)
        gga_chk_str, gga_chk_sum, gga_chk_hex = self.checksum(gpgga_base)
        
        gprmc_str = "${0}*{1}\r\n".format(rmc_chk_str, rmc_chk_hex)
        nmea_string = "${0}*{1}\r\n".format(gga_chk_str, gga_chk_hex)
        return nmea_string, gprmc_str
    
    def dict_to_gsv_gsa(self, sky_dict):
        """
        converts gps json SKY class in to GSV and GSA strings
        :param sky_dict: gpsd json SKY object
        :return: gsv, gsa strings
        """
        gpgsv_strs = []
        
        # get core data
        sats = getattr(sky_dict, 'satellites', 0)
        
        num_sat = len(sats)
        hdop = getattr(sky_dict, 'hdop', '')
        # used in other strings
        self.hdop = hdop
        pdop = getattr(sky_dict, 'pdop', '')
        vdop = getattr(sky_dict, 'vdop', '')
        prn = []
        az = []
        el = []
        snr = []
        used = []
        
        for sat in sats:
            prn.append(getattr(sat, 'PRN', ''))
            az.append(getattr(sat, 'az', ''))
            el.append(getattr(sat, 'el', ''))
            snr.append(getattr(sat, 'ss', ''))
            used.append(getattr(sat, 'used', ''))
        
        gsv_sentences = num_sat / 4
        
        if num_sat % 4:
            gsv_sentences += 1
            for i in range(4 - num_sat % 4):
                prn.append('')
                az.append('')
                el.append('')
                snr.append('')
                used.append('')
        
        # generate gsv string
        for i in range(gsv_sentences):
            gpgsv_string = ["$GPGSV", "{}".format(gsv_sentences), "{}".format(i + 1), "{}".format(num_sat)]
            pos = i * 4
            for s in range(4):
                if isinstance(prn[pos + s], str):
                    dprn = ''
                else:
                    dprn = "{:02}".format(prn[pos + s])
                gpgsv_string.append(dprn)
                gpgsv_string.append("{}".format(el[pos + s]))
                gpgsv_string.append("{}".format(az[pos + s]))
                gpgsv_string.append("{}".format(snr[pos + s]))
            chk_str, chk_sum, chk_hex = self.checksum(",".join(gpgsv_string))
            gpgsv_strs.append("${}*{}\r\n".format(chk_str, chk_hex))
        
        self.num_satellites = used.count(True)
        
        # generat gsa string
        gpgsa_str = ['$GPGSA', 'A', '3']
        for i in range(12):
            if i < len(prn):
                gpgsa_str.append(str(prn[i]))
            else:
                gpgsa_str.append('')
        gpgsa_str.append(str(pdop))
        gpgsa_str.append(str(hdop))
        gpgsa_str.append(str(vdop))
        chk_str, chk_sum, chk_hex = self.checksum(",".join(gpgsa_str))
        gpgsa_string = ("${}*{}\r\n".format(chk_str, chk_hex))
        return gpgsv_strs, gpgsa_string
    
    def dict_to_nmea(self, gpsd_json_dict):
        """
        wrapper for other json to nmea parses, takes any json report and returns a string of valid nmea strings
        :param gpsd_json_dict: gpsd nmea data
        :type gpsd_json_dict: Gps
        :return: list of nmea strings {some strings may be there on list of strings)
        """
        gpgsv_strs = None
        gpgsa_str = None
        gpgga_string = None
        gprmc_str = None
        
        if getattr(gpsd_json_dict, 'class') == u'SKY':
            gpgsv_strs, gpgsa_str = self.dict_to_gsv_gsa(gpsd_json_dict)
        
        elif getattr(gpsd_json_dict, 'class') == u'TPV':
            
            gpgga_string, gprmc_str = self.dict_to_gga_rmc(gpsd_json_dict)
        
        return [gpgga_string, gprmc_str, gpgsv_strs, gpgsa_str]
    
    @staticmethod
    def dms2dd(degrees, minutes, seconds, direction):
        """
        Converts degrees minutes seconds in to latitude or longitude
        :param degrees: degrees
        :type degrees: int
        :param minutes: minutes
        :type minutes: int
        :param seconds: seconds
        :type seconds: float
        :param direction: 'N', 'S', 'E', 'W'
        :type direction: str
        :return: lat or long in decimal
        """
        dd = float(degrees) + float(minutes) / 60 + float(seconds) / (60 * 60)
        if direction == 'S' or direction == 'W':
            dd *= -1
        return dd
    
    @staticmethod
    def dd2dms(deg):
        """
        Take degrees and return degrees minutes seconds
        :param deg: degrees
        :type deg: float
        :return: (degrees, Minutes, Seconds)
        """
        d = int(deg)
        md = abs(deg - d) * 60
        m = int(md)
        sd = (md - m) * 60
        return [d, m, sd]
    
    def parse_dms(self, dms):
        """
        parse a degrees minutes and seconds format lat long
        :param dms: string containg lat long in D,M,S
        :type dms: str
        :return: Decimal Lat, Decimal Long
        """
        parts = re.split('[^\d\w]+', dms)
        lat = self.dms2dd(parts[0], parts[1], parts[2], parts[3])
        lng = self.dms2dd(parts[4], parts[5], parts[6], parts[7])
        
        return lat, lng
    
    def run_file(self, file_name, repeat=False, realtime=False, is_json=False):
        """
        wrapper for running files based on data type NMEA or JSON
        :param file_name: File to read from
        :type file_name: str
        :param repeat: Whether to repeat the file
        :type repeat: bool
        :param realtime: Whether to update the previous time with utc times
        :type realtime: bool
        :param is_json: Is a json formatted file
        :type is_json: bool
        :return: None
        """
        if is_json:
            self.run_file_json(file_name, repeat, realtime)
        else:
            self.run_file_nmea(file_name, repeat, realtime)
    
    def run_file_nmea(self, file_name, repeat=False, realtimes=False):
        """
        Streams nmea GPS data from a file
        :param file_name: name of file containing gps data
        :type file_name: str
        :param realtimes: replace the current date and times in the nmea strings with
        :type realtimes: bool
        :param repeat: Infinitely repeat the file data
        :type repeat: bool
        :return: 0 on completion or exit
        """
        self.start_accept()
        
        self.log.file_console.info("Streaming GPS data from file : {0}".format(file_name))
        run_count = 1
        while True:
            # wait for a connection before starting
            while len(self.connections) == 0:
                time.sleep(1)
            conn_num = 0
            last_time_stamp = -1
            time_stamp = 0
            # open and stream file
            with open(file_name, 'rU') as gps_data:
                gps_data = gps_data.readlines()
                line_count = len(gps_data)
                step_ten = line_count / 10
                complete = 0
                try:
                    for data in gps_data:
                        line_count -= 1
                        if not line_count % step_ten:
                            if complete:
                                self.log.file_console.info("File {}% complete".format(complete))
                            complete += 10
                        # check string starts with valid beginning.
                        gp = re.search('^\$GP', data)
                        gn = re.search('^\$GN', data)
                        # if not valid line check next line
                        if not gp and not gn:
                            continue
                        # slow down data to ~ once a second by pulling time from GGA string
                        if re.search('GGA', data):
                            time_stamp = int(data.split(',')[1].split('.')[0])
                        
                        if time_stamp != last_time_stamp:
                            time.sleep(1)
                            last_time_stamp = time_stamp
                        
                        # fix line endings
                        crlf = re.search('\r\n', data)
                        cr = re.search('\r', data)
                        lf = re.search('\n', data)
                        if not crlf:
                            if cr:
                                data = data.replace('\r', '\r\n')
                            elif lf:
                                data = data.replace('\n', '\r\n')
                        
                        # replace date and or time in string if real time is enabled
                        # todo currently only support s gga, gsv and gll strings, not sure of any others?
                        if realtimes:
                            is_gga = re.search('GGA', data)
                            is_rmc = re.search('RMC', data)
                            is_gll = re.search('GLL', data)
                            if is_gga or is_rmc or is_gll:
                                new_date, new_time = datetime.datetime.utcnow().strftime('%d%m%y,%H%M%S.%f').split(',')
                                new_time = "{0:010.3f}".format(float(new_time))
                                data_split = data.split(',')
                                if is_gga or is_rmc:
                                    data_split[1] = new_time
                                if is_rmc:
                                    data_split[9] = new_date
                                if is_gll:
                                    data_split[5] = new_time
                                new_string, original_checksum, new_checksum = self.checksum(','.join(data_split))
                                data = "${0}*{1}\r\n".format(new_string, new_checksum)
                        
                        if self._print_to_screen:
                            print(data)
                        # send string to connected sockets
                        try:
                            for i, (con, client_add) in enumerate(self.connections):
                                conn_num = i
                                con.sendall(data.encode('utf-8'))
                        except socket.error as exc:
                            # happens when sockets disconnects
                            self.log.file.warning("Socket Error : {}s".format(exc))
                            bad_con, address = self.connections.pop(conn_num)
                            ip, prt = address
                            try:
                                bad_con.close()
                            except socket.error:
                                self.log.file_console.error("Failed to close connection to {}:{}".format(ip, prt))
                            else:
                                self.log.file_console.info("Closed connection to {}:{}".format(ip, prt))
                            finally:
                                # check and wait for connections
                                if not (len(self.connections)):
                                    self.log.file_console.info("No active connections, waiting for connection/s...")
                                while not len(self.connections):
                                    time.sleep(0.5)
                except KeyboardInterrupt:
                    self.log.file_console.warning("Exiting from keyboard interrupt")
                    self.log.file_console.info(
                            "Run Count = {0}, {1}% File sent, Ending GPS Stream of file : {2}".format(run_count,
                                                                                                      complete,
                                                                                                      file_name))
                    raise
                
                if not repeat:
                    self.log.file_console.info("Ending GPS Stream of file : {0}".format(file_name))
                    return 0
                else:
                    self.log.file_console.info(
                            "Run Count {0}, Restarting Stream of file : {1}".format(run_count, file_name))
    
    def run_file_json(self, file_name, repeat=False, realtimes=False):
        """
        Streams gpsd json GPS data from a file
        :param file_name: name of file containing gps data
        :type file_name: str
        :param realtimes: replace the current date and times in the nmea strings with
        :type realtimes: bool
        :param repeat: infinitely repeat the file data
        :type repeat: bool
        :return: 0 on completion or exit
        """
        self.start_accept()
        
        self.log.file_console.info("Streaming GPS data from file : {0}".format(file_name))
        run_count = 1
        while True:
            # wait for a connection before starting
            while len(self.connections) == 0:
                time.sleep(1)
            conn_num = 0
            # open and stream file
            with open(file_name, 'rU') as gps_data:
                gps_data = gps_data.readlines()
                line_count = len(gps_data)
                step_ten = line_count / 10
                complete = 0
                try:
                    for data in gps_data:
                        line_count -= 1
                        if not line_count % step_ten:
                            if complete:
                                self.log.file_console.info("File {}% complete".format(complete))
                            complete += 10
                        # check string starts with valid beginning.
                        
                        if self._print_to_screen:
                            print(data)
                        # send string to connected sockets
                        try:
                            for i, (con, client_add) in enumerate(self.connections):
                                conn_num = i
                                con.sendall(data.encode('utf-8'))
                        except socket.error as exc:
                            # happens when sockets disconnects
                            self.log.file.warning("Socket Error : {}s".format(exc))
                            bad_con, address = self.connections.pop(conn_num)
                            ip, prt = address
                            try:
                                bad_con.close()
                            except socket.error:
                                self.log.file_console.error("Failed to close connection to {}:{}".format(ip, prt))
                            else:
                                self.log.file_console.info("Closed connection to {}:{}".format(ip, prt))
                            finally:
                                # check and wait for connections
                                if not (len(self.connections)):
                                    self.log.file_console.info("No active connections, waiting for connection/s...")
                                while not len(self.connections):
                                    time.sleep(0.5)
                except KeyboardInterrupt:
                    self.log.file_console.warning("Exiting from keyboard interrupt")
                    self.log.file_console.info(
                            "Run Count = {0}, {1}% File sent, Ending GPS Stream of file : {2}".format(run_count,
                                                                                                      complete,
                                                                                                      file_name))
                    raise
                
                if not repeat:
                    self.log.file_console.info("Ending GPS Stream of file : {0}".format(file_name))
                    return 0
                else:
                    self.log.file_console.info(
                            "Run Count {0}, Restarting Stream of file : {1}".format(run_count, file_name))
    
    @staticmethod
    def checksum(sentence):
        """
        Generates Check Sum for nmea strings
        :param sentence: string to check
        :type sentence: str
        :return: stripped string,
                 orginal check sum if on string for validation,
                 calculated checksum of string
        """
        # Remove any newlines
        if re.search("\n$", sentence):
            sentence = sentence[:-1]
        if re.search('\$', sentence):
            sentence = sentence[1:]
        if re.search('\*', sentence):
            nmeadata, cksum = re.split('\*', sentence)
        else:
            nmeadata = sentence
            cksum = None
        
        calc_cksum = 0
        for s in nmeadata:
            calc_cksum ^= ord(s)
        hex_sum = "{:02X}".format(calc_cksum)
        return nmeadata, cksum, hex_sum


def main():
    argv = sys.argv[1:]
    parser = argparse.ArgumentParser(description='gpsd server wrapper to stream gps data from gpsd or file',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-a', '--address', default='0.0.0.0', type=str, metavar='<ip address>',
                        help='IP address of NMEA streamer')
    parser.add_argument('-p', '--port', default=2948, type=int, metavar='<port number>', help='port to send data to')
    parser.add_argument('-j', '--json', default=False, action='store_true',
                        help='output or input is json/dict gpsd default data instead of nmea')
    parser.add_argument('-i', '--input', default=False, metavar='<path/to/file>', type=str,
                        help='send recorded gps data from a file')
    parser.add_argument('-r', '--repeat', default=False, action='store_true',
                        help='repeat file data until ctrl -c otherwise just run once')
    parser.add_argument('-n', '--newtime', default=False, action='store_true',
                        help='used with fake gps data to replace old time and date with current utc times and dates')
    parser.add_argument('-o', '--printout', default=False, action='store_true', help='print output to screen')
    
    args = parser.parse_args(argv)
    server = NMEAServer(args.address, args.port, args.printout)
    if server.success:
        try:
            if args.input:
                server.run_file(args.input, args.repeat, args.newtime, args.json)
            else:
                server.run(args.json)
        except KeyboardInterrupt:
            pass
        finally:
            try:
                server.close()
            except AttributeError:
                pass
            print("Finished")
    else:
        print('Failed to start server')


if __name__ == "__main__":
    main()
